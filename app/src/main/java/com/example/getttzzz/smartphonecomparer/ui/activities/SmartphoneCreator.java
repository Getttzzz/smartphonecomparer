package com.example.getttzzz.smartphonecomparer.ui.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.example.getttzzz.smartphonecomparer.R;
import com.example.getttzzz.smartphonecomparer.db.DBManager;
import com.example.getttzzz.smartphonecomparer.model.Smartphone;
import com.software.shell.fab.ActionButton;

public class SmartphoneCreator extends AppCompatActivity {

    private EditText etBrand;
    private EditText etModel;
    private ActionButton btCreateSmartphone;
    private Button btBack;
    private DBManager dbManager;

    public static void start(Activity pActivity) {
        Intent intent = new Intent(pActivity, SmartphoneCreator.class);
        pActivity.startActivity(intent);
    }



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smartphone_creator);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Create smartphone");

        etBrand = (EditText) findViewById(R.id.asc_et_brand);
        etModel = (EditText) findViewById(R.id.asc_et_model);
        btCreateSmartphone = (ActionButton) findViewById(R.id.asc_bt_create);
        btCreateSmartphone.setOnClickListener(btnCreateSmartphoneListener);
        btBack = (Button) findViewById(R.id.asc_bt_back);
        btBack.setOnClickListener(btnBackListener);

        dbManager = new DBManager(getApplicationContext());
    }

    View.OnClickListener btnCreateSmartphoneListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Smartphone smartphone = new Smartphone();
            smartphone.setBrand(etBrand.getText().toString());
            smartphone.setModel(etModel.getText().toString());
            dbManager.createNewSmartphone(smartphone);

        }
    };

    View.OnClickListener btnBackListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

        }
    };

}
