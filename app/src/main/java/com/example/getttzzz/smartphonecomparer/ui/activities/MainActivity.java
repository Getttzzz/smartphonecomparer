package com.example.getttzzz.smartphonecomparer.ui.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.SparseBooleanArray;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.getttzzz.smartphonecomparer.R;
import com.example.getttzzz.smartphonecomparer.db.DBManager;
import com.example.getttzzz.smartphonecomparer.model.Smartphone;
import com.example.getttzzz.smartphonecomparer.ui.adapters.SmartphoneBaseAdapter;
import com.software.shell.fab.ActionButton;

import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActionButton actionButton;
    private TextView tvBrand;
    private TextView tvModel;
    private ImageView ivImage;
    private ListView lvList;
    private List<Smartphone> mListOfSmartphone;
    private SmartphoneBaseAdapter mSmartphoneBaseAdapter;
    private DBManager dbManager;
    private int mLastClickSmartphone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("List of smartphones");

        View.OnClickListener clickListener = new Clicker();
        actionButton = (ActionButton) findViewById(R.id.action_button);
        actionButton.setImageDrawable(getDrawable(R.drawable.ic_add_white_36dp));
        findViewById(R.id.action_button).setOnClickListener(clickListener);
        tvBrand = (TextView) findViewById(R.id.tvBrand);
        tvModel = (TextView) findViewById(R.id.tvModel);
        ivImage = (ImageView) findViewById(R.id.ivImg);
        lvList = (ListView) findViewById(R.id.am_list_view);
        dbManager = new DBManager(getApplicationContext());
        mLastClickSmartphone = -1;
        fillData();

        lvList.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        lvList.setMultiChoiceModeListener(new MultiChoise());


//        lvList.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
//            @Override
//            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
//                deleteSmartphone(position);
//                Toast.makeText(getApplicationContext(), "Smartphone deleted", Toast.LENGTH_SHORT).show();
//                fillData();
//                return true;
//            }
//        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        fillData();
        mLastClickSmartphone = -1;
    }

    private final class Clicker implements View.OnClickListener {

        @Override
        public void onClick(View v) {
            switch (v.getId()) {

                case R.id.action_button:
                    SmartphoneCreator.start(MainActivity.this);
                    break;
            }
        }
    }

    private void fillData() {
        mListOfSmartphone = dbManager.getAllSmartphone();
        mSmartphoneBaseAdapter = new SmartphoneBaseAdapter(getApplicationContext(), mListOfSmartphone);
        lvList.setAdapter(mSmartphoneBaseAdapter);
    }

    private void deleteSmartphone(int itemId) {
        mListOfSmartphone = dbManager.getAllSmartphone();
        mListOfSmartphone.get(itemId);
        Smartphone s = mSmartphoneBaseAdapter.getItem(itemId);
        dbManager.deleteSmartphone(s.getSmartphoneId());
    }

    private class MultiChoise implements AbsListView.MultiChoiceModeListener{

        final Intent activitySmartphoneComparerIntent = new Intent(MainActivity.this, SmartphoneComparer.class);
        final SparseBooleanArray sparseBooleanArray = lvList.getCheckedItemPositions();
        final int[] arrayPositionsOfSelectedSmartphones = new int[2];

        @Override
        public void onItemCheckedStateChanged(ActionMode mode, int position, long id, boolean checked) {
            Log.i("myLogs", "onItemCheckedStateChanged");
            if (mLastClickSmartphone != position) {

                for (int i = 0; i < lvList.getCount(); i++) {
                    if (sparseBooleanArray.valueAt(i)) {
                        if (sparseBooleanArray.size() == 2) {
                            arrayPositionsOfSelectedSmartphones[i] = lvList.getCheckedItemPositions().keyAt(i);
                        }
                    }
                }
                if (sparseBooleanArray.size() == 2) {
                    activitySmartphoneComparerIntent.putExtra("arr_smart", arrayPositionsOfSelectedSmartphones);
                    startActivity(activitySmartphoneComparerIntent);
                }
            } else {
                Toast.makeText(getApplicationContext(), "It's the same smart, please, select another", Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            Log.i("myLogs", "onCreateActionMode");
            mode.getMenuInflater().inflate(R.menu.menu_control, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            Log.i("myLogs", "onPrepareActionMode");

            return true;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()){
                case R.id.menu_choose:
                    Log.i("myLogs", "CHOOSE");
                    break;
                case R.id.menu_delete:
                    Log.i("myLogs", "DELETE");
                    break;
            }
            return true;
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {

        }
    }


}
