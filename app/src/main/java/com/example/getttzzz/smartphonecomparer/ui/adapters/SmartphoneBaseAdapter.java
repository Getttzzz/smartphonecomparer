package com.example.getttzzz.smartphonecomparer.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import com.example.getttzzz.smartphonecomparer.R;
import com.example.getttzzz.smartphonecomparer.model.Smartphone;

import java.util.List;

public class SmartphoneBaseAdapter extends BaseAdapter {

    private List<Smartphone> mListSmartphones;
    private LayoutInflater mLayoutInflater;
    private Context mContext;

    public SmartphoneBaseAdapter(Context pContext, List<Smartphone> pList) {
        this.mContext = pContext;
        this.mListSmartphones = pList;

        notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return mListSmartphones.size();
    }

    @Override
    public Smartphone getItem(int position) {
        return mListSmartphones.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        ViewHolder viewHolder;
        if (convertView == null) {
            mLayoutInflater = LayoutInflater.from(mContext);
            view = mLayoutInflater.inflate(R.layout.item, parent, false);
            viewHolder = new ViewHolder(view);
            view.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) view.getTag();
        }

        String brand = mListSmartphones.get(position).getBrand();
        String model = mListSmartphones.get(position).getModel();

        viewHolder.tvBrand.setText(brand);
        viewHolder.tvModel.setText(model);

        return view;
    }

    private static class ViewHolder {
        public TextView tvBrand;
        public TextView tvModel;

        public ViewHolder(View v) {
            tvBrand = (TextView) v.findViewById(R.id.tvBrand);
            tvModel = (TextView) v.findViewById(R.id.tvModel);
        }

    }
}
