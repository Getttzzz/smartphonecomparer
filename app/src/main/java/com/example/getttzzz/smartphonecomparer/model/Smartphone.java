package com.example.getttzzz.smartphonecomparer.model;

public class Smartphone {

    private int smartphoneId;
    private String brand;
    private String model;

    public int getSmartphoneId() {
        return smartphoneId;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public void setSmartphoneId(int smartphoneId) {
        this.smartphoneId = smartphoneId;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public void setModel(String model) {
        this.model = model;
    }
}
