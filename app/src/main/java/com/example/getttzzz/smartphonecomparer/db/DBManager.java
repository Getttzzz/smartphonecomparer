package com.example.getttzzz.smartphonecomparer.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.example.getttzzz.smartphonecomparer.model.Smartphone;

import java.util.ArrayList;
import java.util.List;

public class DBManager {

    private DBHelper dbHelper;
    private SQLiteDatabase sqLiteDatabase;
    final String LOG_TAG = "myLogs";

    public DBManager(Context pContext) {
        dbHelper = new DBHelper(pContext);
        sqLiteDatabase = dbHelper.getWritableDatabase();
    }

    public void createNewSmartphone(Smartphone smartphone) {
        long id = sqLiteDatabase.insert(DBHelper.DATABASE_TABLE, null, Mapper.parseSmartphone(smartphone));
        smartphone.setSmartphoneId((int) id);
        Log.d(LOG_TAG, "row add, ID = " + smartphone.getSmartphoneId());
    }

    public void deleteSmartphone(long rowId) {
        sqLiteDatabase.delete(DBHelper.DATABASE_TABLE, DBHelper.COLUMN_ID + "=" + rowId, null);
    }

    public Smartphone getSmartphoneById(long rowId){
        Smartphone smartphone = new Smartphone();
        Cursor cursor = sqLiteDatabase.query(DBHelper.DATABASE_TABLE, new String[]{DBHelper.COLUMN_ID, DBHelper.COLUMN_BRAND, DBHelper.COLUMN_MODEL}, DBHelper.COLUMN_ID + "=" + rowId, null, null, null, null);
        int currentId = cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_ID));
        String currentBrand = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_BRAND));
        String currentModel = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_MODEL));
        smartphone.setSmartphoneId(currentId);
        smartphone.setBrand(currentBrand);
        smartphone.setModel(currentModel);

        return smartphone;
    }

    public List<Smartphone> getAllSmartphone() {
        List<Smartphone> list = new ArrayList<>();
        Cursor cursor = sqLiteDatabase.query(DBHelper.DATABASE_TABLE, new String[]{DBHelper.COLUMN_ID, DBHelper.COLUMN_BRAND, DBHelper.COLUMN_MODEL}, null, null, null, null, null);

        while (cursor.moveToNext()) {
            int currentId = cursor.getInt(cursor.getColumnIndex(DBHelper.COLUMN_ID));
            String currentBrand = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_BRAND));
            String currentModel = cursor.getString(cursor.getColumnIndex(DBHelper.COLUMN_MODEL));
            Smartphone s = new Smartphone();
            s.setSmartphoneId(currentId);
            s.setBrand(currentBrand);
            s.setModel(currentModel);
            list.add(s);
        }

        return list;
    }

}
