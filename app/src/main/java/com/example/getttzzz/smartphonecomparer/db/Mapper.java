package com.example.getttzzz.smartphonecomparer.db;

import android.content.ContentValues;
import android.database.Cursor;

import com.example.getttzzz.smartphonecomparer.model.Smartphone;

public class Mapper {

    public static ContentValues parseSmartphone(Smartphone smartphone) {
        ContentValues CV = new ContentValues();
        CV.put(DBHelper.COLUMN_BRAND, smartphone.getBrand());
        CV.put(DBHelper.COLUMN_MODEL, smartphone.getModel());
        return CV;
    }

//    public static Smartphone parseCursor(Cursor cursor){
//        Smartphone smartphone = new Smartphone();
//
//        int smartphoneIdIndex = cursor.getColumnIndex(DBHelper.COLUMN_ID);
//        int brandIndex = cursor.getColumnIndex(DBHelper.COLUMN_BRAND);
//        int modelIndex = cursor.getColumnIndex(DBHelper.COLUMN_MODEL);
//
//        smartphone.setSmartphoneId(cursor.getInt(smartphoneIdIndex));
//        smartphone.setBrand(cursor.getString(brandIndex));
//        smartphone.setModel(cursor.getString(modelIndex));
//
//        return smartphone;
//    }

}
