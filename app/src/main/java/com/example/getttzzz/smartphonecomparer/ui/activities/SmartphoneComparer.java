package com.example.getttzzz.smartphonecomparer.ui.activities;

import android.app.Activity;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.TextView;

import com.example.getttzzz.smartphonecomparer.R;
import com.example.getttzzz.smartphonecomparer.db.DBManager;
import com.example.getttzzz.smartphonecomparer.model.Smartphone;
import com.example.getttzzz.smartphonecomparer.ui.adapters.SmartphoneBaseAdapter;

import java.util.List;

public class SmartphoneComparer extends AppCompatActivity {

    private TextView tvBrandForLeftSmart;
    private TextView tvModelForLeftSmart;

    private TextView tvBrandForRightSmart;
    private TextView tvModelForRightSmart;

    private DBManager dbManager;
    private List<Smartphone> mListOfSmartphones;
    private SmartphoneBaseAdapter mSmartphoneBaseAdapter;
    private int[] mArrayOfSmartphones;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_smartphone_comparer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        setTitle("Compare smartphone");

        tvBrandForLeftSmart = (TextView) findViewById(R.id.ascomp_tv_left_smart_brand);
        tvModelForLeftSmart = (TextView) findViewById(R.id.ascomp_tv_left_smart_model);
        tvBrandForRightSmart = (TextView) findViewById(R.id.ascomp_tv_right_smart_brand);
        tvModelForRightSmart = (TextView) findViewById(R.id.ascomp_tv_right_smart_model);

        dbManager = new DBManager(getApplicationContext());

        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            mArrayOfSmartphones = bundle.getIntArray("arr_smart");
        }
        mListOfSmartphones = dbManager.getAllSmartphone();
        mSmartphoneBaseAdapter = new SmartphoneBaseAdapter(getApplicationContext(), mListOfSmartphones);
        Smartphone s1 = getSmartphoneByPosition(mArrayOfSmartphones[0]);
        Smartphone s2 = getSmartphoneByPosition(mArrayOfSmartphones[1]);

        fillData(s1.getBrand(), s1.getModel(), s2.getBrand(), s2.getModel());

    }

    private Smartphone getSmartphoneByPosition(int pId) {
        mListOfSmartphones.get(pId);
        Smartphone smartphone = mSmartphoneBaseAdapter.getItem(pId);
        return smartphone;
    }

    private void fillData(String pBrandForLeft, String pModelForLeft, String pBrandForRight, String pModelForRight){
        tvBrandForLeftSmart.setText(pBrandForLeft);
        tvModelForLeftSmart.setText(pModelForLeft);

        tvBrandForRightSmart.setText(pBrandForRight);
        tvModelForRightSmart.setText(pModelForRight);
    }


}
